import logo from './logo.svg';
import './App.css';
import Header from './components/Header'
import style from './style.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom'

import LoginPage from './views/LoginPage'
import TranslationPage from './views/TranslationPage';
import ProfilePage from './views/ProfilePage';

function App() {
  console.log(process.env.REACT_APP_API_KEY)
  return (
    <BrowserRouter>
    
      <div className="App">
        <Header />
        <Routes>
          <Route path="/"  element={<LoginPage />}  />
          <Route path="/Translation"  element={<TranslationPage />}  />
          <Route path="/Profile"  element={<ProfilePage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
