
const apiUrl = process.env.REACT_APP_USER_API;
const apiKey = process.env.REACT_APP_API_KEY;



export  async function checkforUser(username){
    try{

        const response = await fetch(`${apiUrl}?username=${username}`);
        if(!response.ok){
            throw new Error('Could not complete request.')
        }
        
        const data = await response.json();
        return [null, data]
    }
    catch(errors){
       return [ errors.message, [] ];
    }


}
async function createUser(username){
    try{
        const response = await fetch(apiUrl,{
            method: 'POST',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username:username,
                translations: []
            })
        })
        if(!response.ok){
            throw new Error('Could not create user with username ' + username)
        }
        
        const data = await response.json();
        return [null, data]
    }
    catch(errors){
       return [ errors.message, [] ];
    }
}
export   async function loginUser(username){
    const [checkError, user] = await checkforUser(username);

    if(checkError !== null){
        return [checkError, null]
    }

    if(user.length > 0){
        return [null, user.pop()]
    }
    return await createUser(username);


}


export async function addTranslation(user, translation){
    try{
        const response = await fetch(`${apiUrl}/${user.id}`,{
            method: 'PATCH',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username:user.username,
                translations: [...user.translations, translation]
            })
        })  
        if(!response.ok){
            throw new Error('Could not create user with username ' + user.username)
        }
        
        const data = await response.json();
        return [null, data]
    }
    catch(errors){
       return [ errors.message, [] ];
    }
}

export async function clearTranslation(user){
    try{
        const response = await fetch(`${apiUrl}/${user.id}`,{
            method: 'PATCH',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username:user.username,
                translations: []
            })
        })  
        if(!response.ok){
            throw new Error('Could not create user with username ' + user.username)
        }
        
        const data = await response.json();
        return [null, data]
    }
    catch(errors){
       return [ errors.message, [] ];
    }
}