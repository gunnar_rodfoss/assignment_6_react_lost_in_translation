import {NavLink, useNavigate} from 'react-router-dom'
export default function Header(){
    let navigate = useNavigate();
    function logout(){
        localStorage.clear();
        return navigate("/");
    }
    return(
        <div className="header">
            <img id="image" src="/LostInTranslation_Resources/Logo.png" alt="" />
            <h1>Lost in Translation</h1>
            
            

            { (localStorage.getItem('user') !== null ) &&
                <>
                <h1>{localStorage.getItem('username')}</h1>
                <NavLink to="/Translation">
                    <button>Translation</button>
                </NavLink>
                <NavLink to="/Profile">
                    <button>Profile</button>
                </NavLink>
                </>
            }

            <div id="logout" onClick={logout}>
                Log Aout
            </div>
        </div>
    )
}


