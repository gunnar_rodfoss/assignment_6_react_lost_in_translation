import  {useForm } from 'react-hook-form';
import {useState, onChange,} from 'react'
import {useNavigate} from 'react-router-dom'

import TranslationPage from '../views/TranslationPage';

import {loginUser, checkforUser} from '../api/user'

const usernameConfig = {
    required: true,
    minLength: 4
}

 function LoginPage(){
    let navigate = useNavigate();

     const { 
        register, 
        handleSubmit, 
        formState:{ errors} 
       } = useForm()

    async function onSubmit(data){
        console.log(data);
        const [error, user] = await loginUser(data.username)
        
        console.log(user)
        console.log(error)
        console.log(process.env.REACT_APP_API_KEY)
        localStorage.setItem("username", user.username );
        localStorage.setItem("user", JSON.stringify(user) );
        if(error === undefined || error === null ) {
            
            return navigate("/Translation");
        }

     }

     const errorMessage=( ()=>{
        if(!errors.username){
            return null;
        }
        if(errors.username.type === 'required'){
           return <span>Username is required</span>
        }
        if(errors.username.type === 'minLength'){
            return <span>Username must be of minimum lenth 4</span>
         }
    })()
    
    return(
        <>
            <h1>Login Page</h1>
            <div className="container">
            <form id="inputLoginPageField" onSubmit={handleSubmit(onSubmit)}>
                <fieldset >
                    <label htmlFor="username">Username</label>
                    <input type="text" 
                    {...register("username", usernameConfig)} 
                    placeholder="user name" />
                    
                    { errorMessage }
                    
                </fieldset>
                <button type="submit" >Submit</button>
            
                

            </form>
            </div>
        </>
    )
}
export default LoginPage;
