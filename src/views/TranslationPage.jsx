import { useEffect } from "react"

import {useNavigate} from 'react-router-dom'

import {LoginPage} from '../views/LoginPage';

import DisplayWindow from '../components/DisplayWindow'

import {useState} from 'react'

import {addTranslation} from '../api/user'

export default function TranslationPage(){
    let [translation, setTranslation] = useState("");
    let [word, setWord] = useState("");
    let navigate = useNavigate();
    useEffect(()=>{
        if(localStorage.getItem("user") === null || localStorage.getItem("user") === undefined){
            return navigate("/");
        }
    }
        ,[localStorage]);

    function getWord(e){
        setWord(e.target.value)
    }
    async function translate(){
        setTranslation(word)
        let logedInUser = localStorage.getItem('user');
        logedInUser = JSON.parse(logedInUser)
        console.log("logedInUser: ", logedInUser.id)
        const [error, user] = await addTranslation(logedInUser,word)
        //console.log("Translation from trans page ",user)
        localStorage.setItem("user", JSON.stringify(user) );
    }
 
    return(
        <>
            <h1>Translation Page</h1>
            <div>
                <input onChange={getWord} placeholder="Type a word to translate" />
                <button onClick={translate}>Translate</button>
            </div>
            <DisplayWindow signs={Array.from(translation.toLowerCase().replace(/ /g, ""))}/>
        </>
    )
}