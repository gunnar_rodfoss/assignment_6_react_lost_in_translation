import { useEffect } from "react"

import {useNavigate} from 'react-router-dom'

import {useState} from 'react'

import {checkforUser, clearTranslation} from '../api/user'

export default function ProfilePage(){
    const [history, setHistory] = useState([]);
    let navigate = useNavigate();
    useEffect(()=>{
        if(localStorage.getItem("user") === null || localStorage.getItem("user") === undefined){
            return navigate("/");
        }
    }
        ,[localStorage]);
    async function translate(){

        let logedInUser = localStorage.getItem('user');
        logedInUser = JSON.parse(logedInUser)
        const [error, user]= await checkforUser(logedInUser.username);
        
        setHistory(user[0].translations);
    }
    async function clearHistory(){
        let logedInUser = localStorage.getItem('user');
        logedInUser = JSON.parse(logedInUser)
        
        const [error, user] = await clearTranslation(logedInUser)
        
    }

    return(
        <>
            <h1>Profile Page</h1>
            <button onClick={translate}>History</button>
            <button onClick={clearHistory}>Clear History</button>
                {history.map((h,index)=>{
                    return <li key={index}>{h}</li>;
                })}
            
            
        </>
    )
}